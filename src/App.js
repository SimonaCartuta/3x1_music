import './App.css';
import { useEffect, useState } from 'react';
import MIDISounds from 'midi-sounds-react';
import { createBeatsByOriginalValue, createBeatsByNormalisedValue, createBeatsByModValue, createBeatsByModValueWithChords } from './music';

function applyRule(startingValue, spikeArray=[]) {
  let computedValue = startingValue;
  if (!startingValue){return;}

  if (computedValue === 1) {
    return spikeArray;
  } else {
    computedValue = computedValue % 2 === 0 ? computedValue/2 : computedValue*3 + 1;
    spikeArray.push(computedValue);

    applyRule(computedValue, spikeArray);

  }

  return spikeArray;
}

function App() {
  const [musicalOutput, setMusicalOutput] = useState();
  const [threeXOneArray, setThreeXOneArray] = useState([4, 2, 1]);
  const [userSelectedNumber, setUserSelectedNumber] = useState(1);

  useEffect(() => {
    setThreeXOneArray(applyRule(userSelectedNumber));
  }, [userSelectedNumber]);

  const playOriginal = () => {
      const beats = createBeatsByOriginalValue(threeXOneArray);
      musicalOutput.startPlayLoop(beats.beatsList, beats.bpm, beats.density, beats.fromBeat);
	}

  const playNormalised = () => {
      const beats = createBeatsByNormalisedValue(threeXOneArray);
      musicalOutput.startPlayLoop(beats.beatsList, beats.bpm, beats.density, beats.fromBeat);
	}

  const playMod = () => {
    const beats = createBeatsByModValue(threeXOneArray);
    musicalOutput.startPlayLoop(beats.beatsList, beats.bpm, beats.density, beats.fromBeat);
  }

  const playModChords = () => {
    const beats = createBeatsByModValueWithChords(threeXOneArray);
    musicalOutput.startPlayLoop(beats.beatsList, beats.bpm, beats.density, beats.fromBeat);
  }

  const stopInstrument = () => {
    musicalOutput.stopPlayLoop()
	}

  const handleInputChange = (event) => {
    setUserSelectedNumber(event.target.value);
  }

  return (
    <div className="App">
      <form>
        <p>Type a number</p>
        <input type="text" id="nr" name="nr" defaultValue="1" onChange={handleInputChange}/>
        </form>
      <br/>

      <div>{JSON.stringify(threeXOneArray)}</div>


      {/* Music playing Section - move to component */}
        <p className="App-intro">Press Play to play instrument sound.</p>		
	    	<p><button onClick={playOriginal}>Play by original values</button></p>
        <p><button onClick={playNormalised}>Play by normalised values</button></p>
        <p><button onClick={playMod}>Play by mod values</button></p>
        <p><button onClick={playModChords}>Play by mod values with chords</button></p>
        <p><button onClick={stopInstrument}>Stop</button></p>
      	<p>Component</p>
        <MIDISounds 
          ref={(ref) => (setMusicalOutput(ref))} 
          appElementName="root" 
          instruments={[111]} 
          drums={[2,33]} 
			/>	
      <hr/>
      <p>Sources: <a href={'https://www.npmjs.com/package/midi-sounds-react'}>https://www.npmjs.com/package/midi-sounds-react</a></p>
  </div>
  );
}

export default App;
