export function createBeatsByOriginalValue(values) {
   const pitchesByValue = [];

    values.forEach(value => {
       pitchesByValue.push([ [], [[45,[value],1/16]] ]);
   });

   return {
       beatsList: pitchesByValue,
       bpm: 120,
       density: 0.25,
       fromBeat: 1,
   }
     
}

export function createBeatsByNormalisedValue(values) {
    const pitchesByValue = [];
    
     const sqrtValues = values.map(Math.sqrt);
     const maxValue = Math.max(...sqrtValues);
     const normalisedValues = sqrtValues
         .map(v => (v/maxValue)*87)
         .map(v => Math.round(v)+21); 
 
     normalisedValues.forEach(value => {
        pitchesByValue.push([ [], [[45,[value],1/16]] ]);
    });
 
    return {
        beatsList: pitchesByValue,
        bpm: 120,
        density: 0.25,
        fromBeat: 1,
    }
      
 }

 export function createBeatsByModValue(values) {
    const pitchesByValue = [];
    
     const modValues = values
         .map(v => v%87 + 21);
 
     modValues.forEach(value => {
        pitchesByValue.push([ [], [[290,[value],1/32]] ]);
    });
 
    return {
        beatsList: pitchesByValue,
        bpm: 120,
        density: 0.5,
        fromBeat: 1,
    }
      
 }

 export function createBeatsByModValueWithChords(values) {
    const pitchesByValue = [];
    const perGroup = Math.ceil(values.length / 3);

     const modValues = values
         .map(v => v%87 + 21);

    const groupedModValues = new Array(perGroup)
    .fill('')
    .map((_, i) => modValues.slice(i * 3, (i + 1) * 3))
     
    groupedModValues.forEach(groupedModValue =>  pitchesByValue.push([ [], [[270, groupedModValue ,1/16]] ]))
 
    console.log(groupedModValues);
    
    return {
        beatsList: pitchesByValue,
        bpm: 120,
        density: 0.5,
        fromBeat: 1,
    }
 }

 	// musicalOutput.playChordNow(45, [60, 64, 67], 3.5);
    		// musicalOutput.startPlayLoop(
        //   [
        //     [ [], [[45,[60,64,67],1/4] ]],
        //     [ [],[[45,[67,71,62],1/2] ]],
        //     [ [35, 24],[[45,[69,62,66],1/8] ]]
        //   ], 
        //   120, 
        //   0.5,
        //   1);